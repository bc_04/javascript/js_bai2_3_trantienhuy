let main = () => {
  
  let bai_1 = () => {
    /**
   * Input:
   * - pay_per_day = 30
   * - work_day = 40
   * Steps: 
   *  - Declare and Initialize 2 variables to store 2 inputs.
   *  - Declare and Initialize a variable to store output.
   *  - Output = pay_per_day * work_day
   * Output: 1200
   * 
   */
    let pay_per_day_val =
      parseFloat(document.querySelectorAll(".payPerDay")[0].value) || 0;

    let work_day_val =
      parseInt(document.querySelectorAll(".workDays")[0].value) || 0;
    let output = pay_per_day_val * work_day_val
    document.querySelectorAll(".txt-result-bai-1")[0].innerHTML = `${output}`;
  };

  let bai_2 = () => {
    /**
   * Input:
   * - num_1 = 30.7;
   * - num_2 = 40.1;
   * - num_3 = 50.2;
   * - num_4 = 100;
   * - num_5 = 11.7
   * 
   * Steps: 
   *  - Declare and Initialize 5 variables to store all 5 inputs.
   *  - Declare and Initialize a variable to store output.
   *  - Output = (num_1 + num_2 + num_3 + num_4 + num_5)/5;
   * 
   * Output: 46.54
   * 
   */
    let avg_value = 0;
    let num_1 =
      parseFloat(document.querySelectorAll(".bai-tap-2 .num1")[0].value) || 0;
    let num_2 =
      parseFloat(document.querySelectorAll(".bai-tap-2 .num2")[0].value) || 0;
    let num_3 =
      parseFloat(document.querySelectorAll(".bai-tap-2 .num3")[0].value) || 0;
    let num_4 =
      parseFloat(document.querySelectorAll(".bai-tap-2 .num4")[0].value) || 0;
    let num_5 =
      parseFloat(document.querySelectorAll(".bai-tap-2 .num5")[0].value) || 0;
    avg_value = (num_1 + num_2 + num_3 + num_4 + num_5) / 5;
    document.querySelectorAll(
      ".txt-result-bai-2"
    )[0].innerHTML = `${avg_value}`;
  };

  let bai_3 = () => {
    /**
     * convert_rate = 23500
   * Input:
   * - money_to_convert_val = 45.78;
   * 
   * Steps: 
   *  - Declare and Initialize a variable to store money input.
   *  - Declare and Initialize a variable to store output.
   *  - Declare and Initialize a variable to store formatted output Number to a currency system.
   *  - output = (money_to_convert_val * convert_rate);
   * 
   * Output: 46.54
   * 
   */
    const convert_rate = 23500;
    let money_to_convert_val =
      parseFloat(document.querySelectorAll(".money")[0].value) || 0;
    let converted_money_val = money_to_convert_val * convert_rate;
    let formatted_converted_money_val = new Intl.NumberFormat("vn-VN").format(converted_money_val);

    document.querySelectorAll(
      ".txt-result-bai-3"
    )[0].innerHTML = `${formatted_converted_money_val}`;
  };

  let bai_4 = () => {
    /**
     * 
   * Input:
   * - width = 45.78;
   * - height = 80.43;
   * Steps: 
   *  - Declare and Initialize 2 variables to store width and height input.
   *  - Declare and Initialize a variable to store perimeter value.
   *  - Declare and Initialize a variable to store area value.
   *  - perimeter = (width + height) * 2;
   *  - area = width * height;
   * Output: 
   *  - perimeter = 252.42;
   *  - area = 3682.0854;
   * 
   */
    let perimeter = 0,
      area = 0;
    let width_val = parseFloat(document.querySelectorAll(".width").value) || 0;
    let height_val =
      parseFloat(document.querySelectorAll(".height").value) || 0;
    perimeter = (width_val + height_val) * 2;
    area = width_val * height_val;
    document.querySelectorAll(".txt-result-bai-4")[0].innerHTML = `
        Diện tích: ${area}; 
        Chu vi: ${perimeter}
    `;
  };

  let bai_5 = () => {
    /**
     * 
   * Input:
   * - number = 123
   * Steps: 
   *  - Declare and Initialize a variable to store input
   *  - Declare and Initialize a variable to store output.
   *  - output = number / 10 + number % 10;
   * 
   *  Output: 6
   * 
   */
    let num_val = parseInt(document.querySelectorAll(".num")[0].value) || 0;
    let result = 0;
    result = Math.floor(num_val / 10) + (num_val % 10);
    document.querySelectorAll(".txt-result-bai-5")[0].innerHTML = `${result}`;
  };

  document
    .querySelectorAll(".btn-calc-bai-1")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_1();
    });

  // giai bai 2
  document
    .querySelectorAll(".btn-calc-bai-2")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_2();
    });

  // giai bai 3
  document
    .querySelectorAll(".btn-calc-bai-3")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_3();
    });

  // giai bai 4
  document
    .querySelectorAll(".btn-calc-bai-4")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_4();
    });
  // giai bai 5
  document
    .querySelectorAll(".btn-calc-bai-5")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_5();
    });
};

main();
